NAME = draklive
VERSION = 2.6

check:
	perl -cw -I./lib $(NAME)

clean:
	find -name '*~' -exec rm {} \;

dist: dis 
dis: clean
	@git archive --prefix=$(NAME)-$(VERSION)/ HEAD | xz > $(NAME)-$(VERSION).tar.xz; 
	$(info $(NAME)-$(VERSION).tar.xz is ready)
