package MDV::Draklive::Utils;

use MDK::Common;
use common;
use run_program;
use IPC::Open3;
use IO::Select;

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT = qw(directory_usage run_ run_foreach);

sub directory_usage {
    my ($dir, $o_apparent) = @_;
    my $apparent = $o_apparent && "--apparent-size";
    first(split /\s/, `du -s -B 1 $apparent $dir`);
 }

#- expand only if the pattern contains '*'
#- and matches dot characters (like shell dotglob)
sub glob__ {
    my ($pattern) = @_;
    $pattern =~ /\*/ ? glob_($pattern) : $pattern;
}

sub run_ {
    my $options = ref $_[0] eq 'HASH' ? shift @_ : {};
    my @cmd = @_;
    $options->{timeout} ||= 'never';
    if (arch() !~ /^arm/) {
        my $targetarch = delete $options->{targetarch};
        unshift @cmd, 'setarch', $targetarch if $targetarch;
    }
    print STDERR "running " . (exists $options->{root} && "(in chroot) ") . join(' ', @cmd) . "\n";
    run_program::raw($options, @cmd);
}

sub run_foreach {
    my ($foreach, @command) = @_;
    print STDERR "running " . join(' ', @command) . "\n";
    my $pid = open3(my $cmd_in, my $cmd_out, undef, @command);
    my $selector = IO::Select->new($cmd_out);
    while (my @ready = $selector->can_read) {
        foreach my $fh (@ready) {
            local $_ = scalar <$fh>;
            return if /^open3:/;
            $foreach->();
            $selector->remove($fh) if eof($fh);
        }
    }
    close($cmd_out);
    close($cmd_in);
    return waitpid($pid, 0) > 0 && !($? >> 8);
}

sub mtools_run_ {
    local $ENV{MTOOLS_SKIP_CHECK} = 1;
    &run_;
}

sub device_allocate_file {
    my ($device, $size) = @_;
    run_('dd', "of=$device", 'count=0', 'bs=1', "seek=" . removeXiBSuffix($size));
}

#- format $device as type $type
# FIXME: use fs::format
sub device_mkfs {
    my ($device, $type, $o_label, $o_inode_size) = @_;
    if ($type eq 'vfat') {
        run_('mkfs.vfat', if_(defined $o_label, '-n', $o_label), $device);
    } elsif (member($type, 'ext2', 'ext3', 'ext4')) {
        run_("mkfs.$type", "-m", 0,
             if_(defined $o_label, '-L', $o_label),
             if_($o_inode_size, '-I', $o_inode_size),
             if_(!-b $device, '-F'),
             $device);
    } elsif ($type eq 'swap') {
        run_('mkswap', if_(defined $o_label, '-L', $o_label), $device);
    } else {
        die "unable to mkfs for unsupported media type $type\n";
    }
}

1;
