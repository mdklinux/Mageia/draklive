package MDV::Draklive::Live;

use MDK::Common;

sub new {
    my ($class) = @_;
    bless {}, $class;
}

sub default_name_fields {
    my ($live) = @_;
    [ qw(name version product desktop),
      if_($live->{settings}{region} ne 'all', region),
      qw(arch media)
    ];
}

sub get_name {
    my ($live) = @_;
    my $fields = $live->{name_fields} || default_name_fields($live);
    join('-', grep { $_ } @{$live->{settings}}{@$fields});
}

sub get_set_suffix {
    my ($live) = @_;
    $live->{settings}{set} ? "-$live->{settings}{set}" : "";
}

sub get_builddir {
    my ($live) = @_;
    $live->{settings}{builddir} . '/' . $live->get_name . $live->get_set_suffix;
}

sub get_system_root {
    my ($live) = @_;
    $live->{settings}{chroot} . '/' . $live->get_name . $live->get_set_suffix;
}

sub get_media_prefix {
    my ($live, $setting, $o_boot) = @_;
    my $hidden = $live->{system}{hide_media_dirs} && $live->{media}->get_boot_setting('can_hide', { boot => $o_boot });
    $live->{prefix}{media}{$hidden ? "hidden_$setting" : $setting};
}

sub find_kernel {
    my ($live) = @_;
    require bootloader;
    local $::prefix = $live->get_system_root;
    my @kernels = bootloader::get_kernels_and_labels();
    my $kernel;
    if ($live->{system}{kernel}) {
        $kernel = find { $_->{version} eq $live->{system}{kernel} } @kernels;
        $kernel or die "kernel $live->{system}{kernel} can not be found\n";
    }
    $kernel ||= first(@kernels) or die "no kernel can be found\n";
}

sub get_initrd_name {
    my ($live) = @_;
    'initrd-' . $live->find_kernel->{version} . '.img';
}

sub get_lib_prefix {
    my ($live) = @_;
    my $lib_prefix = find { glob($live->get_system_root . $_ . '/libc.so.*') } qw(/lib64 /lib);
    $lib_prefix or die 'unable to find system libraries in /lib or /lib64';
    $lib_prefix;
}

1;
