package MDV::Draklive::Storage;

use detect_devices;

our %storage_types = (
    cdrom => {
        modules => 'bus/firewire|usb disk/firewire|hardware_raid|ide|sata|scsi|usb',
        media_modules => 'disk/cdrom',
        fs => 'iso9660',
        can_hide => 0,
        source => 'LABEL=MDVCDROOT',
        read_only => 1,
        detect => \&detect_devices::burners,
        create => \&main::create_cdrom_master,
        format => \&main::format_cdrom_device,
        record_needs_master => 1,
        record => \&main::record_cdrom_master,
        replicator => \&main::create_cdrom_replicator,
        record_replicator => \&main::record_cdrom_replicator,
    },
    usb => {
        modules => 'bus/usb disk/usb',
        media_modules => 'disk/raw',
        fs => 'vfat',
        can_hide => 1,
        bootloader => 'grub',
        source => 'LABEL=MDVUSBROOT',
        detect => sub { grep { detect_devices::isKeyUsb($_) } detect_devices::get() },
        create => \&main::create_disk_master,
        format => \&main::format_disk,
        record => \&main::record_usb_master,
        image => \&main::create_disk_image,
        replicator => \&main::create_usb_replicator,
        record_replicator => \&main::record_usb_replicator,
    },
    harddisk => {
        fs => 'ext4',
        bootloader => 'grub',
        source => 'LABEL=MDVROOT',
        create => \&main::create_disk_master,
        format => \&main::format_disk,
        record => \&main::record_harddisk_master,
        image => \&main::create_disk_image,
    },
);

1;
