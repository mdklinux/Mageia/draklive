package MDV::Draklive::Progress;

use POSIX qw(strftime);

sub new {
    my ($class, $total, $time, $o_exp_divide) = @_;
    bless {
        total => $total,
        current => 0,
        start_time => $time,
        exp_divide => $o_exp_divide,
        maxl => length($total) - $o_exp_divide,
    }, $class;
}

sub show {
    my ($progress, $time) = @_;
    my $elapsed_time = $time - $progress->{start_time};
    my $eta = $progress->{current} ? int($elapsed_time*$progress->{total}/$progress->{current}) : -1;
    printf("\r%3d%% (%$progress->{maxl}s/%-$progress->{maxl}s), %8s/%8s (ETA)",
           int(100*$progress->{current}/$progress->{total}),
           (map { substr($_, 0, length($_)-$progress->{exp_divide}) } $progress->{current}, $progress->{total}),
           (map { POSIX::strftime("%H:%M:%S", gmtime($_)) } $elapsed_time, $eta));
}

sub end {
    my ($_progress) = @_;
    print "\n";
}

1;
