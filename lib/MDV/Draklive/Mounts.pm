package MDV::Draklive::Mounts;

use MDK::Common;

my $dir_distrib_sqfs = {
    mountpoint => '/distrib',
    type => 'squashfs',
    path => '/distrib',
    # perl -MMDK::Common -e 'print map_index { (32767 - $::i) . " $_" } grep { !m,^/(?:dev|proc|sys|live/distrib), } uniq(<>)' < bootlog.list > config/distrib.sort
    sort => "config/distrib.sort",
    build_from => '/',
};
my $dir_memory = {
    mountpoint => '/memory',
    type => 'tmpfs',
    mount_opts => 'mode=755',
};

my $dir_modules = {
    mountpoint => '/modules',
    type => 'modules',
    path => '/modules',
    list => 'modules.lst',
};

#- use distro default
our $default = {
    dirs => [],
};

our $simple_union = {
            root => '/union',
            overlay => 'overlay',
            dirs => [
                {
                    mountpoint => '/media',
                    type => 'plain',
                },
                $dir_memory,
            ],
        };

our $squash_rw_union = {
            root => '/union',
            overlay => 'overlay',
            dirs => [
                $dir_distrib_sqfs,
                {
                    mountpoint => '/media/system',
                    type => 'plain',
                },
            ],
        };

sub volatile_squash_union {
            my ($o_modules) = @_;
            {
                root => '/union',
                overlay => 'overlay',
                dirs => [
                    $dir_distrib_sqfs,
                    if_($o_modules, $dir_modules),
                    $dir_memory,
                ],
            };
}

sub squash_union {
            my ($default_size, $o_min_size, $o_modules) = @_;
            {
                root => '/union',
                overlay => 'overlay',
                dirs => [
                    $dir_distrib_sqfs,
                    if_($o_modules, $dir_modules),
                    {
                        mountpoint => '/system',
                        type => 'loopfs',
                        pre_allocate => $default_size,
                        if_(defined $o_min_size, min_size => $o_min_size),
                        fs => 'ext2',
                        path => '/system'
                    },
                    {
                        mountpoint => '/system',
                        type => 'tmpfs',
                        fallback => 1,
                    },
                ],
            };
}

1;
