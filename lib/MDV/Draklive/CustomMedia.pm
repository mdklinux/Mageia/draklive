package MDV::Draklive::CustomMedia;

sub nfs_media {
    my ($module, $client, $path) = @_;
    (
        fs => 'nfs',
        modules => 'nfs',
        extra_modules => [ $module ],
        pre => "ifconfig eth0 $client up",
        source => $path,
    );
}

1;
