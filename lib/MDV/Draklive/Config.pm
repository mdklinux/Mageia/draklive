package MDV::Draklive::Config;

use MDK::Common;
use Pod::Usage;
use Cwd 'getcwd';

#- we bless Media objects here
use MDV::Draklive::Media;
#- these modules can be used from config files
use MDV::Draklive::Mounts;
use MDV::Draklive::CustomMedia;

our $default_config_root = '/etc/draklive';
our $default_config_path = 'config/live.cfg';
our $default_settings_path = 'config/settings.cfg';

sub read_config {
    my ($live, $config_root, $config_path, $settings_path) = @_;

    if ($config_path && -e getcwd() . '/' . $config_path) {
        $config_root = getcwd();
    }
    print "using $config_root as directory root\n";

    add2hash($live->{settings} ||= {}, { getVarsFromSh($config_root . '/' . $settings_path) }) if $settings_path;
    if ($config_path) {
        #- don't use do(), since it can't see lexicals in the enclosing scope
        my $cfg = eval(cat_($config_root . '/' . $config_path)) or die "unable to load $config_path: $@\n";
        put_in_hash($live, $cfg);
        print "loaded $config_path as config file\n";
    }
    $live->{settings}{config_root} = $config_root;
}

sub check_config {
    my ($live) = @_;
    unless (keys(%$live)) {
        warn "no live definition\n";
        Pod::Usage::pod2usage();
    }
    #- check for minimum requirements
    ref $live->{media} && $live->{media}{storage} or die "no media storage definition\n";
    ref $live->{system} or die "no system definition\n";
}

sub complete_config {
    my ($live) = @_;

    my $default_prefix = {
        build => {
            boot => '/boot',
            files => '/files',
            dist => '/dist',
            initrd => '/initrd',
            loopbacks => '/loopbacks',
            modules => '/modules',
            scripts => '/scripts',
        },
        media => {
            boot => '/boot',
            hidden_boot => '/.boot',
            loopbacks => '/loopbacks',
            hidden_loopbacks => '/.loopbacks',
            mnt => '/media',
        },
        live => {
            mnt => '/live',
        },
    };

    #- set unsupplied config dirs
    add2hash($live->{prefix}{$_} ||= {}, $default_prefix->{$_}) foreach keys %$default_prefix;

    $live->{settings}{builddir} ||= '/var/lib/draklive/build';
    $live->{settings}{chroot} ||= '/var/lib/draklive/chroot';

    $live->{settings}{arch} ||= chomp_(`rpm --eval '%{_target_cpu}'`);
    $live->{media}{title} ||= "live";

    $_ = MDV::Draklive::Media::new($_) foreach (
        $live->{media},
        ($live->{replicator} ? $live->{replicator}{media} : ()),
        ($live->{oem_rescue} ? $live->{oem_rescue}{media} : ()),
    );

    mkdir_p($live->get_builddir);
    mkdir_p($live->get_system_root);
    $live->{mnt} ||= $live->get_builddir . "/mnt";
}

sub dump_config {
    my ($live) = @_;
    use Data::Dumper;
    print Data::Dumper->Dump([ $live ], [ "live" ]);
}

1;
