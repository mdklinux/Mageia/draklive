package MDV::Draklive::Media;

use MDK::Common;
use MDV::Draklive::Storage;
use POSIX;
use common;

sub new {
    my ($media) = @_;

    bless $media, 'MDV::Draklive::Media';

    $media->{partitions} ||= [ { mntpoint => '/' } ];

    foreach my $mntpoint (qw(/ OEM_RESCUE)) {
        my $part = find { $_->{mntpoint} eq $mntpoint } @{$media->{partitions}};
        $part->{fs_type} ||= $media->get_media_setting('fs');
        if (my $label = $mntpoint eq '/' && $media->get_media_label) {
            $part->{device_LABEL} ||= $label;
        }
    }

    $media->{boot_entries} ||= [ '' => '' ];

    $media;
}

sub get_initrd_path {
    my ($media) = @_;
    '/' . $media->{storage} . '/initrd.gz';
}

#- mainly for storage-specific subroutines
sub get_storage_setting {
    my ($media, $setting) = @_;
    $MDV::Draklive::Storage::storage_types{$media->{storage}}{$setting};
}

#- for actions that support an optional boot storage type
sub get_boot_setting {
    my ($media, $setting, $opts) = @_;
    $opts->{boot} ? $MDV::Draklive::Storage::storage_types{$opts->{boot}}{$setting} : get_media_setting($media, $setting);
}

#- for user-customisable media setting, that can override storage setting
sub get_media_setting {
    my ($media, $setting) = @_;
    $media->{$setting} || $media->get_storage_setting($setting);
}

sub get_media_fs_module {
    my ($media) = @_;
    my $fs = $media->get_media_setting('fs');
    $fs eq 'iso9660' ? 'isofs' : $fs eq 'ext2' ? @{[]} : $fs;
}

sub get_media_label {
    my ($media) = @_;
    first($media->get_media_setting('source') =~ /^LABEL=(.*)$/);
}

sub get_media_source_for_nash {
    my ($media) = @_;
    my $label = $media->get_media_label;
    #- strip vfat labels to 11 chars and upper-case it
    $label && $media->get_media_setting('fs') eq 'vfat' ?
      'LABEL=' . uc(substr($label, 0, 11)) :
      $media->get_media_setting('source');
}

sub find_partition_index {
    my ($media, $mntpoint) = @_;
    eval { find_index { $_->{mntpoint} eq $mntpoint } @{$media->{partitions}} };
}

sub find_boot_partition_index {
    my ($media) = @_;
    $media->find_partition_index('/boot') || $media->find_partition_index('/');
}

sub supplement_slash_size {
    my ($media, $total_size) = @_;
    my $correction = 1.2;
    my $slash = find { $_->{mntpoint} eq '/' } @{$media->{partitions}};
    $slash->{size} ||= POSIX::ceil($total_size * $correction / $common::SECTORSIZE);
}

1;
