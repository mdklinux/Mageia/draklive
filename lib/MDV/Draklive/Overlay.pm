package MDV::Draklive::Overlay;

use MDV::Draklive::Loopback;

our %overlay = (
    unionfs => {
        modules => [ qw(unionfs) ],
        mount => sub {
            my ($live) = @_;
            #- build dirs list: "dir1=ro:dir2:ro:dir3=rw"
            my $dirs = join(':',
                            map {
                                $_->{list} ? "\$(cat $live->{prefix}{live}{mnt}/$_->{list})" :
                                  "$live->{prefix}{live}{mnt}$_->{mountpoint}=" .
                                    (!$loop_types{$_->{type}}{read_only} && !$_->{read_only} ? 'rw' : 'ro');
                            } reverse grep { !$_->{fallback} } @{$live->{mount}{dirs} || []});
            "sh -c 'mount -o dirs=$dirs -t unionfs unionfs $live->{prefix}{live}{mnt}$live->{mount}{root}'";
        },
    },
);

1;
