package MDV::Draklive::StorageFS;

use MDK::Common;

my %storage_fs;
%storage_fs = (
    generic => {
        mount => sub {
            my ($live, $media) = @_;
            my @mount_options = (
                if_($media->get_media_setting('read_only'), "ro"),
                grep { $_ } $media->get_media_setting('mount_options'),
            );
            'nash-mount' . if_(@mount_options, " -o " . join(",", @mount_options)) . " -t " . $media->get_media_setting('fs') .
              " " . $media->get_media_source_for_nash . " $live->{prefix}{live}{mnt}$live->{prefix}{media}{mnt}";
        },
    },
    nfs => {
        files => [ '/sbin/ifconfig', '/bin/mount' ], #- needed to mount NFS (with nolock)
        mount => sub {
            my ($live, $media) = @_;
            '/bin/mount -n -o ro,nolock -t nfs ' . $media->get_media_source_for_nash . " $live->{prefix}{live}{mnt}$live->{prefix}{media}{mnt}";
        }
    },
    ext2 => {
        files => [ '/sbin/fsck.ext2' ],
        mount => sub {
	    my ($live, $media) = @_;
            my $fsck = "/bin/fsck.ext2";
            my $source = $media->get_media_setting('source');
            qq(sh -c '$fsck -a $source || $fsck -y $source'),
            $storage_fs{generic}{mount}->($live, $media);
        },
    },
    vfat => {
	#files => [ '/sbin/dosfsck' ],
	mount => sub {
	    my ($live, $media) = @_;
	    #$storage_fs{generic}{mount}->($live, $media),
	    #qq(awk '\$2 == "$live->{prefix}{live}{mnt}$live->{prefix}{media}{mnt}" { system("umount " \$1 "; mount; echo dosfsck -a " \$1 "; dosfsck -a " \$1) }' /proc/mounts),
            $storage_fs{generic}{mount}->($live, $media);
        },
    },
);

sub get_files {
    my ($fs) = @_;
    @{$storage_fs{$fs} && $storage_fs{$fs}{files} || []};
}

sub get_mount {
    my ($fs) = @_;
    $storage_fs{exists $storage_fs{$fs}{mount} ? $fs : 'generic'}{mount};
}

1;
